# -*- coding: utf-8 -*-


def log_call(function):
    def wrapper(*args, **kwargs):
        output = function(*args, **kwargs)
        print('[{}.{}] input: {} {}, output: {}'.format(
            args[0].__class__.__name__,
            function.__name__,
            args[1:],
            kwargs,
            output))
        return output
    return wrapper


def date_range_to_zoom_level(date_from, date_to):
    return 0  # TODO: dopasowanie najlepszej wartości zoom_level
