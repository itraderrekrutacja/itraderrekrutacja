# -*- coding: utf-8 -*-
import datetime
import threading
import time

from mocks.db_connection import DbConnectionMock
from mocks.external_api import ExternalApiMock
from mocks.websocket_connection import WebsocketConnectionMock

from modules.quotes_api import QuotesApi
from modules.quotes_archive import QuotesArchive
from modules.quotes_loader import QuotesLoader
from modules.quotes_stream import QuotesStream


if __name__ == "__main__":
    db_connection = DbConnectionMock()
    external_api = ExternalApiMock()
    api = QuotesApi(db_connection)

    for stock in ('CDP', 'WIG20'):
        loader = QuotesLoader(stock, db_connection, external_api)
        stream = QuotesStream(stock, db_connection)
        archive = QuotesArchive(stock, db_connection)

        loader_thread = threading.Thread(target=loader.run)
        stream_thread = threading.Thread(target=stream.run)
        archive_thread = threading.Thread(target=archive.run)

        loader_thread.start()
        stream_thread.start()
        archive_thread.start()

        stream.connect(WebsocketConnectionMock())  # user1
        stream.connect(WebsocketConnectionMock())  # user2

    time.sleep(90)

    now = datetime.datetime.now()
    quotes_data = api.get_quotes(
        stock_id='CDP',
        date_from=now-datetime.timedelta(seconds=40),
        date_to=now-datetime.timedelta(seconds=30)
    )
    print(quotes_data)
