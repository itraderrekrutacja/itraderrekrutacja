# -*- coding: utf-8 -*-
import time

from utils import log_call


class WebsocketConnectionMock(object):

    @log_call
    def emit(self, message):
        time.sleep(0.1)
