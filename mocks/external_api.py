# -*- coding: utf-8 -*-
import datetime
import time
import random

from utils import log_call


class ExternalApiMock(object):

    @log_call
    def next_message(self, stock_id):
        time.sleep(0.1)
        if random.randint(0, 1):
            return {
                'stock_id': stock_id,
                'quote': random.random()*2 + 10,
                'datetime': datetime.datetime.now(),
            }
