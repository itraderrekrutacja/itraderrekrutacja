# -*- coding: utf-8 -*-
import time

from utils import log_call


class DbConnectionMock(object):

    def __init__(self):
        self.quotes_stream_queue = []
        self.quotes_archive_queue = []
        self.quotes_archive = []

    @log_call
    def insert_into(self, table_name, values):
        time.sleep(0.1)
        getattr(self, table_name).append(values)

    @log_call
    def select_stream_queue_message(self, stock_id):
        time.sleep(0.5)
        return next((item for item in self.quotes_stream_queue if item['stock_id'] == stock_id), None)

    @log_call
    def select_archive_queue_messages(self, stock_id):
        time.sleep(0.5)
        return [item for item in self.quotes_archive_queue if item['stock_id'] == stock_id]

    @log_call
    def delete_from(self, table_name, value):
        time.sleep(0.05)
        try:
            getattr(self, table_name).remove(value)
        except ValueError:
            print('========', value, getattr(self, table_name))

    @log_call
    def select_last_archive_item(self, stock_id, level):
        time.sleep(0.2)
        items = [item for item in self.quotes_archive if item['stock_id'] == stock_id and item['level'] == level]
        if items:
            return items[-1]

    @log_call
    def select_archive_items(self, stock_id, level, date_from):
        time.sleep(0.2)
        return [item for item in self.quotes_archive
                if item['stock_id'] == stock_id
                and item['level'] == level
                and item['date_to'] > date_from]

    @log_call
    def select_archive_items_to_api(self, stock_id, level, date_from, date_to):
        """
        Zwraca elementy archiwum zawierające żądany zakres dat.
        """
        time.sleep(0.2)
        return [item for item in self.quotes_archive
                if item['stock_id'] == stock_id
                and item['level'] == level
                and item['date_to'] > date_from
                and item['date_from'] < date_to]
