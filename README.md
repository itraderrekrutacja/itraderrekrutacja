Architektura rozwi�zania


1. Zewn�trzne API kurs�w gie�dowych
Zak�adam, �e dla ka�dego instrumentu jest osobne po��czenie,
kt�re co kilka sekund wysy�a wiadomo�� z aktualnym kursem i dat�.


2. Modu� pobieraj�cy kursy (quotes_loader)
Pobiera kursy z zewn�trznego API.
Pobrane dane wysy�a do kolejek "Modu�u notowa� na �ywo" i "Modu�u archiwum notowa�".
Osobny proces dla ka�dego instrumentu.


3. Modu� notowa� na �ywo (quotes_stream)
Pobiera kursy z kolejki i udost�pnia je na �ywo w formie WebSocketu.


4. Modu� archiwizacji notowa� (quotes_archive)
Przyjmuje dane z kolejki osobnej dla ka�dego instrumentu.
Zapisuje w bazie danych notowania dla r�nych odcink�w czasu, aby "Modu� pobierania notowa� z archiwum"
m�g� zwraca� optymaln� liczb� warto�ci w zale�no�ci od poziomu przybli�enia wykresu.


5. Modu� pobierania notowa� z archiwum (quotes_api)
Udost�pnia API dla frontendu zwracaj�ce notowania dla wybranego instrumentu w wybranym zakresie dat.
W zale�no�ci od poziomu przybli�enia wykresu pobiera optymaln� liczb� warto�ci.


6. Frontend
Pobiera notowania za pomoc� API "Modu�u pobierania notowa� z archiwum" i wy�wietla wykres.
Frontend musi zna� godziny pracy gie�dy, aby poprawnie wy�wietli� wykres.
