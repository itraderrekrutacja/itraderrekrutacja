# -*- coding: utf-8 -*-
from utils import date_range_to_zoom_level


class QuotesApi(object):

    def __init__(self, db_connection):
        self.db_connection = db_connection

    def get_quotes(self, stock_id, date_from, date_to):
        zoom_level = date_range_to_zoom_level(date_from, date_to)

        archive = self.db_connection.select_archive_items_to_api(stock_id, zoom_level, date_from, date_to)

        response = self._get_quotes_from_archive(archive)
        if self._needs_unarchived_quotes(archive, date_to):
            response.update(self._get_unarchived_quotes(date_from, date_to, stock_id, zoom_level))
        return response

    def _get_quotes_from_archive(self, archive):
        response = {}
        for item in archive:
            response.update({
                quote['datetime'].strftime("%Y-%m-%d.%H:%M:%S"): quote['quote'] for quote in item['quotes']
            })
        return response

    def _needs_unarchived_quotes(self, archive, date_to):
        return not archive or archive[-1]['date_to'] < date_to

    def _get_unarchived_quotes(self, date_from, date_to, stock_id, zoom_level):
        """
        Pobiera kursy z zakresu dat od ostatniej archiwizacji do daty date_to.
        """
        return {}  # TODO
