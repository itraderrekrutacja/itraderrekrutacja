# -*- coding: utf-8 -*-


class QuotesStream(object):

    def __init__(self, stock_id: str, db_connection):
        self.stock_id = stock_id
        self.db_connection = db_connection
        self.websocket_connections = []

    def run(self):
        while True:
            message = self.db_connection.select_stream_queue_message(self.stock_id)
            if not message:
                continue
            self.emit_message(message)
            self.db_connection.delete_from('quotes_stream_queue', message)

    def connect(self, websocket_connection):
        self.websocket_connections.append(websocket_connection)


    def disconnect(self, websocket_connection):
        self.websocket_connections.remove(websocket_connection)

    def emit_message(self, message):
        for connection in self.websocket_connections:
            connection.emit(message)
