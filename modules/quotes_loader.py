# -*- coding: utf-8 -*-


class QuotesLoader(object):

    def __init__(self, stock_id: str, db_connection, external_api):
        self.stock_id = stock_id
        self.db_connection = db_connection
        self.external_api = external_api

    def run(self):
        while True:
            message = self.external_api.next_message(self.stock_id)
            if not message:
                continue

            self.db_connection.insert_into('quotes_stream_queue', message)
            self.db_connection.insert_into('quotes_archive_queue', message)
