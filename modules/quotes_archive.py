# -*- coding: utf-8 -*-
import datetime
import time


CONFIG = [{  # ustawiam małe wartości interval dla celów testowych
    'level': 0,  # poziom przyblizenia
    'interval': datetime.timedelta(seconds=10),  # jak dlugie odcinki w archiwum
}, {
    'level': 1,
    'interval': datetime.timedelta(seconds=30),
    'factor': 3,  # co ktory kurs zostawic wzgledem poprzedniego zoom level
}, {
    'level': 2,
    'interval': datetime.timedelta(seconds=90),
    'factor': 3,
}]


class QuotesArchive(object):

    def __init__(self, stock_id: str, db_connection):
        self.stock_id = stock_id
        self.db_connection = db_connection

    def run(self):
        while True:
            time.sleep(1)
            self.archive()

    def archive(self):
        now = datetime.datetime.now()
        for level in CONFIG:
            if level['level'] == 0:
                self.archive_main_level(level, now)
            else:
                self.archive_level(level, now)

    def archive_main_level(self, level, now):
        """
        Archiwizuje pełne dane we fragmentach do szybkiego odczytu.
        """
        last_item = self.db_connection.select_last_archive_item(self.stock_id, level['level'])
        # TODO: przechowywanie daty ostatniej archiwizacji w lepszym miejscu

        messages = self.db_connection.select_archive_queue_messages(self.stock_id)
        if messages and (not last_item or last_item['date_to'] + level['interval'] > now):
            # TODO: obsluga pierwszego uruchomienia/kolejki duzo dluzszej niz interval
            self.db_connection.insert_into('quotes_archive', {
                'date_from': messages[0]['datetime'],
                'date_to': now,
                'quotes': messages,
                'stock_id': self.stock_id,
                'level': 0,
            })

            for message in messages:
                self.db_connection.delete_from('quotes_archive_queue', message)

    def archive_level(self, level, now):
        """
        Archiwizuje dane dla oddalonych wykresów (np. dla miesiąca/roku).
        Zostawia część danych względem poprzedniego poziomu przybliżenia.
        Podejrzewam, że w praktyce używa się średniej albo kursu z jakiejś konkretnej godziny.
        """
        last_item = self.db_connection.select_last_archive_item(self.stock_id, level['level'])
        if not last_item or last_item['date_to'] + level['interval'] > now:
            date_from = last_item['date_to'] if last_item else datetime.datetime(2018, 1, 1)
            archive_items = self.db_connection.select_archive_items(self.stock_id, level['level']-1, date_from)
            quotes = []
            for item in archive_items:
                quotes.extend(item['quotes'])
            quotes = quotes[::level['factor']]
            self.db_connection.insert_into('quotes_archive', {
                'date_from': date_from,
                'date_to': now,
                'quotes': quotes,
                'stock_id': self.stock_id,
                'level': level['level'],
            })
